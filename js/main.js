const baseUrl = "http://127.0.0.1:5500/";

let url = window.location.href;
url = new URL(url);

function currency(number = 0, prefix = "Rp.") {
    return prefix + ' ' + (number.toLocaleString());
}


function rating(number = "") {
    html = "";
    for (let i = 0; i < number; i++) {
        html += '<span class="fa fa-star"></span>';
    }

    return html;
}

function getTags(arr = [], separator = ", ") {
    return arr.join(separator);
}


$("#search-trigger").click(function (e) {
    e.preventDefault();
    $(".search-wrapper").toggleClass('hidden');
})





function addCart() { }
function removeCart() { }
function updateCart() { }

let counter;
let cart = $.getJSON(baseUrl + 'cart.json', function (data) {
    html = "";
    let qty, total, grandTotal = 0;
    counter = 0;
    $.each(data, function (key, val) {
        total = (val.price * val.qty);
        grandTotal = grandTotal + total;
        html +=
            '<tr>' +
            ' <td align="center">' +
            ' <div class="cart-thumb ">' +
            '<img src="' + val.images + '" alt="Women Bag " class="img-responsive ">' +
            '</div>' +
            '</td>' +
            '<td>' +
            ' <h4>' + val.title +
            '<span class="td-actions ">' +
            '<a href=" " class="btn bg-sm btn-danger">' +
            '<span class="fa fa-trash "></span>' +
            '</a>' +
            ' </span>' +
            '</h4>' +
            '<p>' + val.mini_description + '</p>' +
            '</td>' +
            '<td>' +
            ' <input type="number" class="form-control " value="' + val.qty + '" min="1" max="9 ">' +
            '</td>' +
            '<td class="text-right"> ' + currency(val.price) + '</td>' +
            '<td class="text-right">' + currency(total) + '</td>' +
            '</tr>';
        counter++;
    });
    $(".cart-wrapper").append(html);
    $("#grandtotal").text(currency(grandTotal));

    $("span.qty").text(counter);
});

console.log(counter);





